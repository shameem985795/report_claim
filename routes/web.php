<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ComplainController;
use App\Http\Controllers\ComplainReplyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin', function () {
    return view('auth.login');
});


Route::get('/', function () {
    return view('reports.index');
});
Route::get('/verify', function () {
    return view('reports.verify');
})->name('reports.verify');
Route::get('/success', function () {
    return view('reports.success');
})->name('reports.success');
Route::get('/report_list', function () {
    return view('reports.report_list');
})->name('reports.report_list');
Route::get('/report_view', function () {
    return view('reports.view');
})->name('reports.view');
// Route::get('/all_reports', function () {
//     return view('reports.all_reports');
// })->name('reports.all_reports');


Route::post('report/store',[ComplainController::class,'store'])->name('reports.store');
Route::post('report/success',[ComplainController::class,'success'])->name('reports.success');
Route::get('report/verify',[ComplainController::class,'verify']);
Route::get('report_list',[ComplainController::class,'report_list'])->name('reports.list');
Route::get('report_view/{id}',[ComplainController::class,'report_view'])->name('reports.view');


Route::post('report_reply',[ComplainReplyController::class,'complain_reply'])->name('reports.reply');
Route::get('all_reports',[ComplainReplyController::class,'all_reports']);


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
