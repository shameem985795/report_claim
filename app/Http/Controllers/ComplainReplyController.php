<?php

namespace App\Http\Controllers;

use App\Models\Complain;
use App\Models\ComplainReply;
use App\Models\ComplainUserMap;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ComplainReplyController extends Controller
{
    public function complain_reply(Request $request)
    {
        ComplainReply::create([
            'complain_id'=>$request->complain_id,
            'details'=>$request->details,
            'user_id'=>$request->user_id
        ]);


      if($request->complain_id){
        Complain::where('id',$request->complain_id)->update([
            'status'=>'2',
            'assigned_user_id'=>$request->user_id,
            'assigned_date'=>Carbon::today()
            ]);

      }
        ComplainUserMap::create([
            'complain_id'=>$request->complain_id,
            'user_id'=>$request->user_id,
        ]);


        return redirect('report_list');

    }


    public function all_reports(Request $request)
    {

    // $reports = Complain::find(1)->complainType->complain_type;
    //$reports->find(1)->complainType->complain_type

        // $reports = ComplainReply::find(1)->complain->name;
        // $report = ComplainReply::find(1)->user->name;



        // foreach($reports as $report){
        //     dd($report->find($report->complain_id)->complain->name);
        // }

        // if($request->complain_id){
        //     $reports = ComplainReply::all();
        //     return view('reports.all_reports',compact('reports'));
        // }

        $reports = Complain::all();
        // dd($reports);

       return view('reports.all_reports',compact('reports'));
    }
}



