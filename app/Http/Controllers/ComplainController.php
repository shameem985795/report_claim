<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Complain;
use App\Models\ComplainType;

class ComplainController extends Controller
{

public function store(Request $request)
{
   $complain_type = ComplainType::create([
       'complain_type'=>$request->complain_type
   ]);


   Complain::create([
       'name'=>$request->name,
       'mobile_no'=>$request->mobile_no,
       'email'=>$request->email,
       'complain_type_id'=>$complain_type->id,
       'complain_details'=>$request->complain_details
   ]);

   $complain_type_id = $complain_type->id;
   return view('reports.verify',compact('complain_type_id'));

}

public function success(Request $request)
{
    $complain_type_id =$request->complain_type_id;
    $token =$request->token;

    $complain_data= Complain::where('complain_type_id','=',$complain_type_id)->get();
    foreach($complain_data as $data){

         if($data['token'] == $token){
            Complain::where('complain_type_id',$complain_type_id)->update(['status' => '1']);;
            return view('reports.success');
        }
    }

    return view('reports.verify',compact('complain_type_id'));
}


public function report_list()
{
    // $reports = Complain::find(1)->complainType->complain_type;
    //$reports->find(1)->complainType->complain_type

    $reports = Complain::all();
    return view('reports.report_list',compact('reports'));
}


public function report_view($id)
{
    $report = Complain::find($id);
    return view('reports.view',compact('report'));
}





}
