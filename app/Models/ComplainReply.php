<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplainReply extends Model
{
    use HasFactory;

    protected $table = 'complain_replies';
    protected $fillable = [
        'id',
        'complain_id',
        'details',
        'user_id'

    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function complain()
    {
        return $this->belongsTo('App\Models\Complain');
    }

}
