<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplainType extends Model
{
    use HasFactory;

    protected $table = 'complain_types';
    protected $fillable = [
        'id',
        'complain_type',
        'status'
    ];

    // public function complains()
    // {
    //     return $this->hasOne(Complain::class,'complain_type_id');
    // }
}
