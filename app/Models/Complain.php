<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    use HasFactory;

    protected $table  = 'complains';
    protected $fillable = [
        'id',
        'name',
        'mobile_no',
        'email',
        'complain_type_id',
        'complain_details',
        'assigned_user_id',
        'assigned_date',
        'status',
    ];


    public function complainType()
    {
        return $this->belongsTo('App\Models\ComplainType');
    }
}
