<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplainUserMap extends Model
{
    use HasFactory;

    protected $table = 'complain_user_maps';
    protected $fillable =
    [
        'id',
        'complain_id',
        'user_id'
    ];
}
