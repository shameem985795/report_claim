@extends('layouts.master')


@section('content')


<div class="container mt-5">
    {{-- <div class="col-md-6 offset-5 text-dark">
        <p class="h3">Complain</p>
    </div> --}}

        <div class="row mt-3">
            <div class="col-md-6 offset-3">
                <form action="{{ route('reports.success') }}" method="POST">
                    @csrf
                    @method('POST')
                        <input type="hidden" value="{{ $complain_type_id??$complain_type_id }}" name="complain_type_id">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Your Verify Code</label>
                            <input type="text" name="token" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
        </div>

</div>

@endsection

@push('css')
<style>
    body{
        background-image: linear-gradient(to right,  rgb(255, 255, 255), rgb(247, 239, 239));
    }
</style>

@endpush
