@extends('layouts.app')


@section('content')


<div class="container mt-5">
    <div class="col-md-6 offset-5 text-dark">
        <p class="h3">Reports</p>
    </div>

        <div class="row mt-3">
            <div class="col-md-10 offset-1">
                <table class="table table-responsive-lg table-bordered table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Mobile </th>
                        <th scope="col">Email</th>
                        <th scope="col">Complain Type</th>
                        <th scope="col">Status</th>
                        <th scope="col">Date</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>


                    @foreach ($reports as $report)


                    <?php
                    // echo "<pre>";
                    //     var_dump($report->find($report->complain_id)->complain->complain_type_id );

                    ?>
                      {{-- <tr>
                        <td scope="row">{{ $report->find($report->complain_id)->complain->name?$report->find($report->complain_id)->complain->name:$report->name }}</td>
                        <td>{{ $report->find($report->complain_id)->complain->mobile?$report->find($report->complain_id)->complain->mobile:$report->mobile  }}</td>
                        <td>{{  $report->find($report->complain_id)->complain->email?$report->find($report->complain_id)->complain->email:$report->email }}</td>
                        <td>{{  $report->find($report->complain_id)->complain->complain_type_id?$report->find($report->complain_id)->complain->complain_type_id:$report->complain_type_id }}</td>
                        <td>{{ $report->find($report->complain_id)->complain->status?$report->find($report->complain_id)->complain->status:$report->status }}</td>
                        <td>{{ $report->find($report->complain_id)->complain->assigned_date }}</td>
                        <td>
                            <a role="btn" href="{{}}">view</a>
                        </td>
                      </tr> --}}

                      <tr>
                        <td scope="row">{{ $report->name }}</td>
                        <td>{{$report->mobile_no  }}</td>
                        <td>{{ $report->email }}</td>
                        <td>{{ $report->complain_type_id }}</td>
                        <td>{{$report->status }}</td>
                        <td>{{ $report->assigned_date}}</td>
                        <td>
                            <a role="btn" href="{{}}">view</a>
                        </td>
                      </tr>


                      @endforeach

                    </tbody>
                  </table>
            </div>
        </div>

</div>

@endsection

@push('css')
<style>
    body{
        background-image: linear-gradient(to right,  rgb(255, 255, 255), rgb(247, 239, 239));
    }
</style>

@endpush
