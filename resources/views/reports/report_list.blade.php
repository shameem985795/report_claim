@extends('layouts.app')


@section('content')



<div class="container mt-3">
    <nav class="navbar navbar-expand-lg navber-inverse navbar-light bg-light">
        {{-- <a class="navbar-brand" href="#">Report List</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> --}}

        {{-- <div class="collapse navbar-inverse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active mr-5">
              <a class="nav-link " href="#">Report <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active mr-5">
              <a class="nav-link " href="#">Report List <span class="sr-only">(current)</span></a>
            </li>

          </ul>

        </div> --}}

    </nav>

    <div class="row mt-5">


        <div class="col-md-6 offset-5 text-dark">
            <p class="h3">Complain List</p>
        </div>


        <div class="col-md-8 offset-2">
            <table class="table table-responsive-lg table-bordered table-hover text-center">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Mobile </th>
                    <th scope="col">Email</th>
                    <th scope="col">Complain Type</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>

                    @foreach ($reports as $report)
                        <tr>
                            <td scope="row">{{ $report->name }}</td>
                            <td>{{ $report->mobile_no }}</td>
                            <td>{{ $report->email }}</td>
                            <td>{{ $report->find($report->id)->complainType->complain_type }} </td>
                            <td>
                                <a role="btn" href="{{ route('reports.view',$report->id) }}">view</a>
                            </td>
                        </tr>
                  @endforeach

                </tbody>
              </table>
        </div>
    </div>

</div>


</div>


@endsection

@push('css')
<style>
    body{
        background-image: linear-gradient(to right,  rgb(255, 255, 255), rgb(247, 239, 239));
    }
</style>

@endpush
