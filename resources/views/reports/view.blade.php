
@extends('layouts.master')


@section('content')


<div class="container mt-5">
    <div class="col-md-6 offset-5 text-dark">
        <p class="h3">Complain</p>
    </div>
    <form action="{{ route('reports.reply') }}" method="POST">
        @csrf
        @method('POST')
        <div class="row mt-3">

            <div class="col-md-3 offset-3 ">



                    <input type="hidden" name="user_id" value="{{ Auth()->user()->id }}">
                    <input type="hidden" name="complain_id" value="{{$report->id }}">
                    <input type="hidden" name="report_reply_id" value="{{$report->id }}">


                    <div class="form-group">
                      <label class="h6" for="exampleInputEmail1">Name</label>
                      <p>{{ $report->name }} </p>
                    </div>

                    <div class="form-group">
                      <label class="h6" for="exampleInputEmail1">Mobile</label>
                      <p>{{ $report->mobile_no }}</p>                    </div>

                    <div class="form-group">
                      <label class="h6" for="exampleInputEmail1">Email</label>
                      <p>{{ $report->email }}</p>                       </div>

                    <div class="form-group">
                      <label class="h6" for="exampleInputEmail1">Complain Type</label>
                      <p>  {{ $report->find($report->id)->complainType->complain_type }} </p>
                        </div>

                    <div class="form-group">
                      <p class="h6">Description</p>
                      <p>
                        {{ $report->complain_details }}
                      </p>
                    </div>


                    <button type="submit" class="btn btn-primary">save</button >
                    {{-- <a href="#" class="btn btn-primary">close</a >
                    <a href="#" class="btn btn-primary">save & close</a > --}}


            </div>
            <div class="col-md-3 offset-1">
                <button id="btn" class="btn btn-primary mb-2">comments this reports</button>
                <textarea name="details" id="textarea" cols="40" rows="10" style="display: none"></textarea>
            </div>


        </div>
    </form>
</div>


@endsection

@push('css')
<style>
    body{
        background-image: linear-gradient(to right,  rgb(255, 255, 255), rgb(247, 239, 239));
    }
</style>

@endpush

@push('js')
<script>
    $(document).ready(function(){
        $("#btn").click(function(e){
            e.preventDefault();
            $("#btn").hide();
            $("#textarea").show();
        })
    })
</script>

@endpush
