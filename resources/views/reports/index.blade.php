@extends('layouts.master')


@section('content')


<div class="container mt-5">
    <div class="col-md-6 offset-5 text-dark">
        <p class="h3">Complain</p>
    </div>

        <div class="row mt-3">
            <div class="col-md-6 offset-3">
                <form action="{{ route('reports.store') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
                      <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Mobile</label>
                      <input type="tel" name="mobile_no" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                      <label for="exampleInputEmail1">Complain Type</label>
                      <input type="text" name="complain_type" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                    </div>

                    <div class="form-group">
                      <p>Description</p>
                      <textarea rows="3" cols="50" name="complain_details"></textarea>
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
        </div>

</div>

@endsection

@push('css')
<style>
    body{
        background-image: linear-gradient(to right,  rgb(255, 255, 255), rgb(247, 239, 239));
    }
</style>

@endpush
