<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplainRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complain_replies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('complain_id');
            $table->text('details',150);
            $table->unsignedBigInteger('user_id');


            $table->foreign('complain_id')->references('id')->on('complains');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complain_replies');
    }
}
