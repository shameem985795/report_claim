<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComplainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complains', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->integer('mobile_no');
            $table->string('email',50);
            $table->unsignedBigInteger('complain_type_id')->nullable();
            $table->text('complain_details');
            $table->unsignedBigInteger('assigned_user_id')->nullable();
            $table->date('assigned_date')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->timestamps();

            $table->foreign('complain_type_id')->references('id')->on('complain_types');
            $table->foreign('assigned_user_id')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complains');
    }
}
